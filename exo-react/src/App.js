import React, { Component } from 'react';
import Movies from './Movies';
import './App.css';


class App extends Component {
  state= {
    movies: [{name: "fight club", imgUrl: "http://t1.gstatic.com/images?q=tbn:ANd9GcTdLTXtlwgmOU3b9iXMccLmWsFVJlBxwG3PBodcNk2O3SfJx1Jx", description:"Le narrateur, sans identité précise, vit seul, travaille seul, dort seul, mange seul ses plateaux-repas pour une personne comme beaucoup d'autres personnes seules qui connaissent la misère humaine, morale et sexuelle. C'est pourquoi il va devenir membre du Fight club, un lieu clandestin ou il va pouvoir retrouver sa virilité, l'échange et la communication. Ce club est dirigé par Tyler Durden, une sorte d'anarchiste entre gourou et philosophe qui prêche l'amour de son prochain."},
        {name: "princesse mononoké", imgUrl: "https://static.fnac-static.com/multimedia/images_produits/ZoomPE/6/5/8/5050582477856/tsp20130903004933/Les-Fils-de-l-Homme-Edition-Collector.jpg", description:"La nouvelle Audi RS 4 Avant dévoilée en septembre dernier par le constructeur d'Ingolstadt, dans le cadre du Salon de Francfort, est disponible à la vente depuis le 9 novembre à partir de 92 000 €"},
        {name: "Les fils de l'homme", imgUrl: "http://fr.web.img6.acsta.net/pictures/16/05/26/09/54/204765.jpg", description:"Ce modèle exceptionnel qui figure parmi les trois premiers exemplaires de la DB4 réalisés avec une conduite à droite, a été préparé et modifié par un amateur éclairé de la marque, Richard Williams, lequel la garda en sa possession durant 37 ans."},
    ],

      currentMovie: 0

  };

  componentDidMount() {
    const { timer } = this.props;
    setInterval(() => {
      const newIndex = (this.state.currentMovie + 1) % this.props.slides.length
        this.setState({current_movie: new_index})

    })
  }

  render() {
      const { movies } = this.state;
      return (
          <div className="App">
              {
                movies.map((movie, index) =>{
                  return (
                    <Movies key={index} movie={movie}  />
                  )
                })
              }
          </div>
      );
  }
}

export default App;
