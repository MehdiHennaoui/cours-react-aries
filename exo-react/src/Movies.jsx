import React, { Component } from 'react'
import { Image, Grid } from 'semantic-ui-react';

class Movies extends Component {


    render() {
        const { movie } = this.props;
        return (
            <Grid stackable className="imgBackround">
            <Image
                fluid
                src={movie.imgUrl}
            />
            <h1>{movie.name}</h1>
            <p>{movie.description}</p>
            </Grid>
        );
    }
}

export default Movies;