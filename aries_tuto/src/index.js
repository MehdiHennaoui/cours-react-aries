import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.jsx';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App title="welcome to react" color="red" />, document.getElementById('root'));
registerServiceWorker();
