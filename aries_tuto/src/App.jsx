import React, { Component } from 'react';
import './App.css';
import Student  from './Student';
import { Button, Grid, Form, Input } from 'semantic-ui-react'

class App extends Component {

    state = {
        current_student: {name: "", age: 0},
        student_name: "",
        students: [], // {name: String, age: Number}
        color: 'red'
    };

    handleChangeStudent = (attr, event) => {
        const { current_student } = this.state;
        current_student[attr] = event.target.value;
        this.setState({current_student});
    };


    addStudent = (event) => {
        event.preventDefault();
        let { students, current_student} = this.state;
        students.push(current_student);
        this.setState({
            students: students,
            current_student: {name: "", age:""}
        })
    };

    removeStudent = (index) => {
        let {students} = this.state;
         students.splice(index, 1);
         this.setState({
             students
         })
    };

    onPop = () => {
        alert('A base de pop pop pop');
    }

    render() {
        const {current_student, students } = this.state;
        const { } = this.props;
        return (
            <Grid>
                <Grid.Column>
                <h1>MON SUPER SYSTEME DE GESTION D'ETUDIANTS</h1>
                <Form>
                    <Input
                        placeholder="Nom de l'étudiant"
                        onChange={(e) => this.handleChangeStudent('name', e)}
                        type="text"
                        value={current_student.name}
                    />
                    <Input
                        placeholder="Age de l'étudiant"
                        onChange={(e) => this.handleChangeStudent('age', e)}
                        type="number"
                        value={current_student.age}
                    />
                    {current_student.name && <button className="ui button" onClick={this.addStudent}>Ajouter Etudiant</button>}
                    {students.map((stu, index) => {
                        return(
                            <Student index={index} onRemove={this.removeStudent} key={index + stu} student={stu} onPop={this.onPop}/>
                        );
                    })}
                </Form>
                </Grid.Column>
            </Grid>
        );
    }
}

export default App;
