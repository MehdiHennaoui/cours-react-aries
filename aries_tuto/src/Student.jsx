import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';

export default class Student extends Component {
    remove= () => {
        console.log('appel de remove student', this.props.student.name);
        this.props.onRemove(this.props.index);
    };
    pop= () => {
      this.props.onPop();
    };

    render() {
        const { student } = this.props;
        return(
            <div>
                <p>{student.name} _ {student.age}</p>
                <Button onClick={this.remove} content="Supprimer"/>
                <Button onClick={this.pop} content="Pop" />
            </div>
                )
    }
}